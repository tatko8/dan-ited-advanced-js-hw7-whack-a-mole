class Cell {
  constructor(row, col) {
    this.row = row;
    this.col = col;
    this.element = document.createElement('td');
    this.element.addEventListener('click', this.handleClick.bind(this));
  }
  
  handleClick() {
    if (this.element.classList.contains('blue')) {
      this.element.classList.remove('blue');
      this.element.classList.add('green');
      game.updateScore(1);
    } else {
      this.element.classList.add('red');
      game.updateScore(-1);
    }
  }
  
  reset() {
    this.element.classList.remove('blue', 'green', 'red');
  }
}
  
class Game {
  constructor(boardSize) {
    this.boardSize = boardSize;
    this.cells = [];
    this.score = 0;
    this.scoreElement = document.getElementById('score');
    this.gameBoard = document.getElementById('board');
  }
  
  createBoard() {
    for (let row = 0; row < this.boardSize; row++) {
      const newRow = document.createElement('tr');
      const rowCells = [];
  
    for (let col = 0; col < this.boardSize; col++) {
      const cell = new Cell(row, col);
      newRow.appendChild(cell.element);
      rowCells.push(cell);
    }
      this.cells.push(rowCells);
      this.gameBoard.appendChild(newRow);
    }
  }
  
  updateScore(points) {
    this.score += points;
    this.scoreElement.textContent = 'Score: ' + this.score;
  
    if (this.score >= (this.boardSize * this.boardSize) / 2) {
      this.endGame();
    }
  }
  
  startTimer(difficulty) {
    this.timer = setInterval(this.showRandomCell.bind(this), difficulty);
  }
  
  stopTimer() {
    clearInterval(this.timer);
  }
  
  showRandomCell() {
    const randomRow = Math.floor(Math.random() * this.boardSize);
    const randomCol = Math.floor(Math.random() * this.boardSize);
    const cell = this.cells[randomRow][randomCol];
    cell.reset();
    cell.element.classList.add('blue');
  }
  
  endGame() {
    this.stopTimer();
    this.gameBoard.removeEventListener('click', this.handleCellClick);
    let result = '';
    if (this.score > (this.boardSize * this.boardSize) / 2) {
      result = 'The player win!';
    } else {
      result = 'The player lost!';
    }
    alert(result);
  }
}
  
let game;
  
function startGame(difficulty) {
  game = new Game(10);
  game.createBoard();
  game.startTimer(getDifficultyLevel(difficulty)); 
}
  
function getDifficultyLevel(difficulty) {
  switch (difficulty) {
    case 'easy':
      return 1500;
    case 'medium':
      return 1000;
    case 'hard':
      return 500;
    default:
      return 1500;
  }
}
  